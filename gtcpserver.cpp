#include "gtcpserver.h"
#include "clientwork.h"

gTcpServer::gTcpServer(int serverPort, QObject *parent):QTcpServer(parent),m_serverPort(serverPort)
{
    if(listen(QHostAddress::Any, m_serverPort) != true)
    {
        qDebug() << errorString() <<serverError();
    }
    else
    {
        qDebug() << "listen success at port" << m_serverPort;
    }
}

gTcpServer::~gTcpServer()
{
    for(int i = 0; i < m_clientList.count(); i++)
    {
        delete m_clientList.at(i);
    }
}

Client *gTcpServer::currentClient(const QString clientIp, const qint16 port)
{
    foreach (Client *var, m_clientList)
    {
        if(var->peerAddress().toString().contains(clientIp) && (var->peerPort() == port))
        {
            return var;
        }
    }
    return nullptr;
}

void gTcpServer::slot_txs(const Client *client, const QByteArray &data)
{
    emit txDatas(client, data);
}

void gTcpServer::updateClient(const Client *client)
{
    qDebug() << "updateClient";
    emit ClientMiss(const_cast<Client *> (client));
    delete client;
    m_clientList.removeAt(m_clientList.indexOf(const_cast<Client *>(client)));
}

void gTcpServer::incomingConnection(qintptr handle)
{
    Client *client = new Client(this, handle);
    if(client)
    {
        m_clientList.append(client);
        emit newClientOk(client, m_clientList.indexOf(client));
    }
}