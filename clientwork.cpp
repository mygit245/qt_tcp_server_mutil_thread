#include "clientwork.h"

Client::Client(gTcpServer *server, qintptr fd):m_incomefd(fd),m_server(server)
{
    m_workthread = new QThread();
    if(m_workthread && m_server)
    {
        setSocketDescriptor(m_incomefd);
        moveToThread(m_workthread);
        connect(m_server, SIGNAL(txDatas(const Client *, QByteArray)), this, SLOT(sendDatas(const Client *, const QByteArray)));
        connect(this, SIGNAL(readyRead()), this, SLOT(recvDatas()));
        connect(this, SIGNAL(dataReady(const Client *, QByteArray)), m_server, SIGNAL(rxDatas(const Client *,const QByteArray)));
        m_workthread->start();
    }
    connect(this, SIGNAL(disconnected()), this, SLOT(slot_disconnect()));
    connect(this, SIGNAL(sgl_disconnect(const Client*)), m_server, SLOT(updateClient(const Client*)));
}

Client::~Client()
{
    m_workthread->quit();
    m_workthread->wait();
    delete m_workthread;
    m_server = nullptr;
}
/**
 * @brief Client::recvDatas 接收数据函数,发送数据手动的信号给服务器
 */
void Client::recvDatas()
{
    QByteArray datas = readAll();
    qDebug() << "recv datas" << datas;
    emit dataReady(this, datas);
}
/**
 * @brief Client::sendDatas 发送数据槽函数
 * @param client 服务器端传递过来的client方便和本client对比，如果是本client则发送数据，否则什么页不做
 * @param datas 要发送的数据
 */
void Client::sendDatas(const Client *client,const QByteArray &datas)
{
    if(client == this)
    {
        write(datas);
    }
}
/**
 * @brief Client::slot_disconnect 发送信号告诉server，已经关闭了
 */
void Client::slot_disconnect()
{
    qDebug() << "slot_disconnect";
    emit sgl_disconnect(this);
}