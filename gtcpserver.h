#ifndef GTCPSERVER_H
#define GTCPSERVER_H

#include <QObject>
#include <QTcpServer>
#include <QDebug>
#include <QList>
#include <QTcpSocket>
#include <QThread>
#include <QMap>

namespace Gqa
{
    class gTcpServer;
}
class Client;
class gTcpServer : public QTcpServer
{
    Q_OBJECT

public:
     explicit gTcpServer(int serverPort, QObject *parent = nullptr);
    ~gTcpServer();
    Client *currentClient(const QString clientIp, const qint16 port);//通过ip和端口查找client
public slots:
    void slot_txs(const Client *client, const QByteArray &data);//发送数据
    void updateClient(const Client *client);
protected:
    void incomingConnection(qintptr handle) override;
signals:
    void txDatas(const Client *client, const QByteArray data); //发送数据的信号
    void rxDatas(const Client *client, const QByteArray data); //数据接收的信号
    void newClientOk(const Client *client, int idx);//新的客户端ok信号
    void ClientMiss(Client *client);//新的客户端ok信号
private:
    int m_serverPort; //本服务器的端口
    QList<Client *> m_clientList;//保存客户端
};

#endif // GTCPSERVER_H