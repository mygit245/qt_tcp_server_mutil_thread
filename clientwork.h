#ifndef CLIENTWORK_H
#define CLIENTWORK_H

#include <QObject>
#include <QTcpServer>
#include <QDebug>
#include <QList>
#include <QTcpSocket>
#include <QThread>
#include "gtcpserver.h"

namespace Gqa
{
    class Client;
}
class gTcpServer;
class Client : public QTcpSocket
{
    Q_OBJECT
public:
    Client(gTcpServer *server, qintptr fd); //不可以有父类
    virtual ~Client();
signals:
    void dataReady(const Client *client, const QByteArray &datas);
//    void newClientOk(const QString &msg, const qint16 port);
    void sgl_disconnect(const Client *client);
public slots:
    void recvDatas();
    void sendDatas(const Client *client,const QByteArray &datas);
    void slot_disconnect();
private:
    qintptr m_incomefd; //本client用的fd
    QThread *m_workthread; //指向client所用到的线程
    gTcpServer *m_server; //指向服务器
};

#endif // CLIENTWORK_H