#-------------------------------------------------
#
# Project created by QtCreator 2018-05-31T21:14:28
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tcpServerTest
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    clientwork.cpp \
    gtcpserver.cpp

HEADERS  += widget.h \
    clientwork.h \
    gtcpserver.h

FORMS    += widget.ui
