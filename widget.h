#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "clientwork.h"
#include "gtcpserver.h"
#include <QListWidget>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

public slots:
    void slot_newClientOk(const Client *, int idx);
    void slot_showRecv(const Client *client, const QByteArray datas);
private:
//     Client *currentClient(const QStringList clientMsg);
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();
    void slot_clientMiss(Client *);
signals:
    void sendDatas(const Client *client, const QByteArray datas);

private:
    Ui::Widget *ui;
    gTcpServer *server;
    QMap<Client *, QListWidgetItem *> clientMap;
};

#endif // WIDGET_H
