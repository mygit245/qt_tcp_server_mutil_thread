#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    server = nullptr;
}

Widget::~Widget()
{
    delete ui;
}

void Widget::slot_newClientOk(const Client *client, int idx)
{
    QString msg = client->peerAddress().toString().remove("::ffff:") + QString(":%1").arg(client->peerPort());
    qDebug() << "slot_newClientOk" << msg << "idx:" << idx;
    QListWidgetItem *item = new QListWidgetItem(msg);
    ui->listWidget->insertItem(idx, item);
    qDebug() << ui->listWidget->item(idx) << "itemcount" << ui->listWidget->count() << ui->listWidget->item(idx);
    clientMap[const_cast<Client *>(client)] = ui->listWidget->item(idx);
}

void Widget::slot_showRecv(const Client *client, const QByteArray datas)
{
    ui->textBrowser->append("[" + client->peerAddress().toString().remove("::ffff:") + QString(":%1").arg(client->peerPort()) +"]" +  QString::fromLocal8Bit(datas));
}


void Widget::on_pushButton_clicked()
{
    if(!server)
    {
        server = new gTcpServer(7000, this);
        connect(server, SIGNAL(newClientOk(const Client *, int)), this, SLOT(slot_newClientOk(const Client *, int)));
        connect(server, SIGNAL(rxDatas(const Client *,QByteArray)), this, SLOT(slot_showRecv(const Client*,QByteArray)));
        connect(this, SIGNAL(sendDatas(const Client*,QByteArray)), server, SLOT(slot_txs(const Client*,QByteArray)));
        connect(server, SIGNAL(ClientMiss(Client *)), this, SLOT(slot_clientMiss(Client *)));
    }
}

void Widget::on_pushButton_2_clicked()
{
    if(server)
    {
        if(!ui->listWidget->currentItem())
            return;
        QString clientStr = ui->listWidget->currentItem()->text();
        QStringList list = clientStr.split(":");
        QString port = list.at(1);
        Client *clientPtr = server->currentClient(list.at(0), port.toInt());
        if(clientPtr)
        {
             emit sendDatas(clientPtr, ui->lineEdit->text().toLocal8Bit());
        }
    }
}

void Widget::slot_clientMiss(Client *client)
{
    qDebug() << "slot_clientMiss:" << client;
    ui->listWidget->removeItemWidget(clientMap[client]);
    delete clientMap[client];
    clientMap.remove(client);
}
